//
// mi9 coding challenge
// REST api
//

// Use with heroku port
var PORT = process.env.PORT || 3000;

// Modules to handle HTTP requests
require('./src/server')( PORT );