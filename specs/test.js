
var testServer = require('../src/server')(3000);

var mock = require('supertest');
var mock_request = require('./stubs/sample_request.json');
var mock_response = require('./stubs/sample_response.json');


// Run through the HTTP requests
describe("REST API", function(){

	it("should return 404 if a method other than POST is sent", function(done){

		// Send a payload which is not JSON
		mock( testServer ).get('/').expect(400, JSON.stringify({
			error : 'Could not process request: http method POST only'
		})).end(done);

	});

	it("should throw an error if the API recieves non-json payload", function(done){

		// Send a payload which is not JSON
		mock( testServer ).post('/').send('not-json').expect(400, JSON.stringify({
			error : "Could not decode request: JSON parsing failed"
		})).end(done);

	});

	it("should return JSON given a JSON request", function(done){

		var json = '{}';

		// Send a payload which is not JSON
		mock( testServer ).post('/').send('{}').expect('content-type', 'application/json').expect(200, JSON.stringify({response:[]})).end(done);

	});

	it("should transform the request JSON as defined by the response JSON", function(done){

		mock( testServer ).post('/').send( JSON.stringify(mock_request) ).expect('content-type', 'application/json').expect(200, JSON.stringify(mock_response) ).end(done);

	});

});
