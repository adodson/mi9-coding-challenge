var bodyToJSON = require('./utils/bodyToJSON');
var api = require('./api');


// Initiate HTTP module
var http = require('http');

// Expose initialization for the HTTP server
module.exports = function( PORT ){

	// Handle HTTP requests
	var server = http.createServer( app );

	// Create a webserver listening on io port.
	if( PORT ){
		server.listen( PORT );
	}

	return server;
};



//
// App
// Defines how to handle HTTP requests
//
function app( req, res ){
	// How to handle this request?
	res.setHeader('Content-Type', 'application/json');
	
	// If the request METHOD equals POST
	// Check the post body is vald JSON
	// Then expose it to our API
	if( req.method === 'POST' ){

		bodyToJSON( req, function(json){

			if(typeof(json)==='undefined'){
				res.writeHead(400);
				res.write( JSON.stringify({
					error : "Could not decode request: JSON parsing failed"
				}));
			}
			else{
				var converted = api( json );
				res.write( JSON.stringify( converted ) );
			}
			res.end();
		});
	}
	

	// Otherwise this is not an accepted method
	// Return appropriate response code
	else{
		res.writeHead(400);
		res.write( JSON.stringify({
			error : 'Could not process request: http method POST only'
		}));
		res.end();
	}
}
