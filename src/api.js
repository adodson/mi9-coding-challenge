//
// API
// Transforms JSON object
//
module.exports = function(json){

	// Transform the JSON payload
	var response = [];
	
	if( json.payload &&
		json.payload instanceof Array ){

		response = json.payload
		// Filter
		.filter(function(item){
			return item.drm && item.episodeCount > 0;
		})
		// Augment
		.map(function(item){
			return {
				image : item.image.showImage,
				slug : item.slug,
				title : item.title
			};
		});
	}

	return {
		response : response
	};
};