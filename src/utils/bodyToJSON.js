//
// bodytoJSON
// Converts the body of an HTTP request object to JSON.

module.exports = function(req, callback){
	var body = '';
	req.on('data', function(data) {
		body += data;
	});
	req.on('end', function() {
		var json;
		try{
			json = JSON.parse(body);
		}
		catch(e){
			// should not be defined anyway but lets enforece that.
			json = undefined;
		}
		callback( json );
	});
};